import React, { Component } from 'react';

export default class Header extends Component {
  render() {
    return (
      <div className="container px-lg-5">
        <div className="d-flex justify-content-between align-items-center py-2">
          <div className="headerName">Start Boostrap</div>
          <nav className="headerNav">
            <ul className="d-flex">
              <li className="active">
                <a href="#">Home</a>
              </li>
              <li className="mx-3">
                <a href="#">About</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}
