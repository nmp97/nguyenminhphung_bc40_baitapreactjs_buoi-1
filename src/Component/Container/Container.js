import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class Container extends Component {
  render() {
    return (
      <div className="container px-lg-5 text-center">
        <header className="py-5">
          <div className="p-4 p-lg-5 bg-light rounded-3 text-center">
            <div className="m-4 m-lg-5">
              <h1 className="title display-5 fw-bold">A warm welcome!</h1>
              <p className="fs-4">
                Bootstrap utility classes are used to create this jumbotron since the old component
                has been removed from the framework. Why create custom CSS when you can use
                utilities?
              </p>
              <a href="#" className="btn btn-primary btn-lg">
                Call to action
              </a>
            </div>
          </div>
        </header>

        <section className="pt-4">
          <div className="row gx-5">
            <div className="col-6 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                    <FontAwesomeIcon icon="rectangle-list" />
                  </div>
                  <h2 className="fs-4 fw-bold">Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new layout for this template!
                  </p>
                </div>
              </div>
            </div>

            <div className="col-6 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                    <FontAwesomeIcon icon="cloud-arrow-down" />
                  </div>
                  <h2 className="fs-4 fw-bold">Free to download</h2>
                  <p className="mb-0">
                    As always, Start Bootstrap has a powerful collectin of free templates.
                  </p>
                </div>
              </div>
            </div>

            <div className="col-6 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                    <FontAwesomeIcon icon="id-card" />
                  </div>
                  <h2 className="fs-4 fw-bold">Jumbotron hero header</h2>
                  <p className="mb-0">
                    The heroic part of this template is the jumbotron hero header!
                  </p>
                </div>
              </div>
            </div>

            <div className="col-6 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                    <FontAwesomeIcon icon="parachute-box" />
                  </div>
                  <h2 className="fs-4 fw-bold">Feature boxes</h2>
                  <p className="mb-0">
                    We've created some custom feature boxes using Bootstrap icons!
                  </p>
                </div>
              </div>
            </div>

            <div className="col-6 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                    <FontAwesomeIcon icon="code" />
                  </div>
                  <h2 className="fs-4 fw-bold">Simple clean code</h2>
                  <p className="mb-0">
                    We keep our dependencies up to date and squash bugs as they come!
                  </p>
                </div>
              </div>
            </div>

            <div className="col-6 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                    <FontAwesomeIcon icon="pen-to-square" />
                  </div>
                  <h2 className="fs-4 fw-bold">A name you trust</h2>
                  <p className="mb-0">
                    Start Bootstrap has been the leader in free Bootstrap templates since 2013!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
