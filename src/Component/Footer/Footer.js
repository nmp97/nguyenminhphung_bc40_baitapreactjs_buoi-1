import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <div className="py-5 bg-dark text-white text-center">
        <span>Copyright © Your Website 2022</span>
      </div>
    );
  }
}
