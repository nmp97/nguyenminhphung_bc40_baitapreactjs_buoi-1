import './App.css';
import Container from './Component/Container/Container';
import Header from './Component/Header/Header';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faRectangleList,
  faCloudArrowDown,
  faIdCard,
  faParachuteBox,
  faCode,
  faPenToSquare,
} from '@fortawesome/free-solid-svg-icons';
import Footer from './Component/Footer/Footer';

library.add(faRectangleList, faCloudArrowDown, faIdCard, faParachuteBox, faCode, faPenToSquare);

function App() {
  return (
    <div className="App">
      <div className="bg-dark text-white">
        <Header />
      </div>
      <div>
        <Container />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default App;
